
/*=================================
=            Variables            =
=================================*/
let todoInput = document.querySelector('input.todo_input'),
    todoList = document.querySelector('section .container ul'),
    button = document.querySelector('button'),
    removeButton = document.querySelector('button.removeAll');

/*======================================
=            Event listeners            =
======================================*/
button.addEventListener('click', addItem, false);
document.addEventListener('click', removeItem, false);
document.addEventListener('click', editItem, false);
document.addEventListener('DOMContentLoaded', loadValues);
todoInput.addEventListener('keypress', onInputKeypress);
removeButton.addEventListener('click', removeAll);


function onInputKeypress(e) { // Call the addItem() if the user presses enter
  if(e.key === "Enter") {
    addItem();
  }
}

function removeItem(e) {
  if(e.target.className !== 'remove') {
    return
  }

  e.target.parentNode.remove(); // Removes the current item
  saveValues(); // Saves into localstorage
}

function removeAll() { // Removes all Todo list items & Saves it
  let elements = [...document.querySelectorAll('li')];
  elements.forEach(el => el.remove());
  saveValues();
}

function addElement(value, checked = false, index) { // Adds an todo list item to the <ul>
  let removeTodo = document.createElement('img');
  let valueSpan = document.createElement('span');
  let editSpan = document.createElement('img');
  let checkbox = document.createElement('input');
  let label = document.createElement('label');
  let todoValueContent = document.createElement('li');

  removeTodo.setAttribute("src", "images/icon_trash.svg");
  removeTodo.className = 'remove';
  valueSpan.className = 'value';
  valueSpan.innerText = value;
  editSpan.className = 'edit';
  editSpan.setAttribute("src", "images/icon_pencil.svg");
  checkbox.setAttribute("type", "checkbox");
  checkbox.setAttribute("id", index);
  checkbox.checked = checked;
  checkbox.className = 'status';
  label.setAttribute("for", index);
  todoValueContent.appendChild(checkbox);
  todoValueContent.appendChild(label);
  todoValueContent.appendChild(valueSpan);
  todoValueContent.appendChild(removeTodo);
  todoValueContent.appendChild(editSpan);

  todoList.appendChild(todoValueContent);

  if(checked) { // Check if the todo item is completed, if so add Checked class
    valueSpan.classList.add('checked');
  }

  checkbox.addEventListener('change', editStatus);

}

function editItem(e) { // Edits a todolist item
  if(e.target.className !== 'edit') {
    return
  }

  // retrieve the todolist element, removes the span, adds a input with the previous value
  e.target.style.cssText = "opacity: 0; cursor: default;"
  let currentItemParent = e.target.parentNode;
  let currentItem = currentItemParent.querySelector('span.value');
  let currentItemValue = currentItem.innerText;
  let editInput = document.createElement('input');
  editInput.className = 'editValue';
  editInput.setAttribute('type', 'text');

  currentItem.remove();
  currentItemParent.insertBefore(editInput, currentItemParent.querySelector('img.remove'));
  editInput.value = currentItemValue;

  editInput.addEventListener('keypress', saveEditedItem);
}

function editStatus(e) {
  let span = e.target.parentNode.querySelector('span.value');
  span.classList.toggle('checked', e.target.checked)

  saveValues();
}

function saveEditedItem(e) {
  if(e.key !== "Enter") {
    return
  }

  // a new value has been filled into the input field and saves it (removes input and appends a span with the new value)
  let currentItemParent = e.target.parentNode;
  let editButton = currentItemParent.querySelector('img.edit').style.cssText = "opacity: 1; cursor: pointer;";
  let currentItem = currentItemParent.querySelector('input.editValue');
  let currentItemValue = currentItem.value;
  let valueSpan = document.createElement('span');
  let checkbox = currentItemParent.querySelector('input[type="checkbox"]');
  valueSpan.className = 'value';

  if(checkbox.checked) {
    valueSpan.className += ' checked'; // if the status was completed, keep it as it was. Completed.
  }

  currentItem.remove();
  currentItemParent.insertBefore(valueSpan, currentItemParent.querySelector('img.remove'));
  valueSpan.innerText = currentItemValue;

  saveValues();
}

function addItem() { // Retrieves input value and uses it as a parameter for addElement() and refocusses the user on the input
  let todoValue = todoInput.value;

  if(!todoValue) {
    return
  }

  let arrayIndexes;
  let values = localStorage.getItem('todoValues');
  values = JSON.parse(values);

  values.forEach((value, index) => { /* Loop over todo values array and get indexes */
    arrayIndexes = index;
  });

  addElement(todoValue, false, arrayIndexes); /* adds the element with the value from the input, checkbox false by default and the corresponding array index */
  saveValues();
  todoInput.value = "";
  todoInput.focus();
}

function getCurrentValues() { // Loop over all todo list items that are not completed
  let elements = [...document.querySelectorAll('li span.value:not(checked)')];
  return elements.map(el => el.innerText);
}

function getCurrentCompletedValues() { // Loop over all todo list items that are completed
 let elements = [...document.querySelectorAll('li input[type="checkbox"]')];
 return elements.map(el => el.checked);
}

function saveValues() { // Stringifies all completed & non-completed todo list items and saves it to the localstorage
  let values = JSON.stringify(getCurrentValues());
  let valuesCompleted = JSON.stringify(getCurrentCompletedValues());
  localStorage.setItem('todoValues', values);
  localStorage.setItem('todoValuesCompleted', valuesCompleted);
}

function loadValues() { // Load all completed & non-completed todo list items and use addElement() accordingly (when a checkbox has been checked)
  let values = localStorage.getItem('todoValues');
  let valuesCompleted = localStorage.getItem('todoValuesCompleted');

  if(!values) {
    return
  }

  values = JSON.parse(values);
  valuesCompleted = JSON.parse(valuesCompleted);


  values.forEach((value, i) => {
    let checked = valuesCompleted[i];
    let index = i;
    addElement(value, checked, index);
  });
}